//
//  ViewController.m
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 7..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import "ViewController.h"
#import "DustWatchConstants.h"
#import "LocationServices.h"
#import "AQICNAPIs.h"
#import "StringParser.h"

@interface ViewController ()

@end

@implementation ViewController

LocationServices *locationServices;
StringParser *sp;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    locationServices = [[LocationServices alloc] initWithDelegate:self];
    [locationServices startLocationServices];
    sp = [[StringParser alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cityFeedClicked:(id)sender {
    NSString *cityFeedsampleRequest = [NSString stringWithFormat:@"%@%@", AQICN_API_GEO_LAT_LNG_FEED, AQICN_TOKEN];
    NSURL *url = [NSURL URLWithString:cityFeedsampleRequest];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSString *ret = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self.m_resultLabel setText:[NSString stringWithFormat:@"Result : %@", ret]];
}

- (IBAction)getLocationClicked:(id)sender {
    [self.m_resultLabel setText:[NSString stringWithFormat:@"Current Location :%@", locationServices.currentLocation]];
}

- (IBAction)AQIClicked:(id)sender {
    NSString *cityFeedsampleRequest = [NSString stringWithFormat:@"%@%@", AQICN_API_GEO_LAT_LNG_FEED, AQICN_TOKEN];
    NSURL *url = [NSURL URLWithString:cityFeedsampleRequest];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSString *ret = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self.m_resultLabel setText:[NSString stringWithFormat:@"Result : %@", [sp getIAQI:ret]]];
}
@end
