//
//  AppDelegate.h
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 7..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

