//
//  StringParser.h
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 20..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#ifndef StringParser_h
#define StringParser_h

#import <Foundation/Foundation.h>

@interface StringParser : NSObject {
}
- (NSString*)getIAQI:(NSString*)data;
@end


#endif /* StringParser_h */
