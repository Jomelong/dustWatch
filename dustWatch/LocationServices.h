//
//  LocationServices.h
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 20..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#ifndef LocationServices_h
#define LocationServices_h
// LocationServices.h

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface LocationServices : NSObject <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
}

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *currentLocation;

- (LocationServices*)initWithDelegate:(id)delegate;
- (void)startLocationServices;

@end

#endif /* LocationServices_h */
