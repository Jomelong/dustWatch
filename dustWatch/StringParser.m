//
//  StringParser.m
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 20..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StringParser.h"

@implementation StringParser

- (NSString*)getIAQI:(NSString*)data {
    NSError *jsonError;
    NSData *objectData = [data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    
    NSDictionary *dataDic = [json objectForKey:@"data"];
    NSString *aqi = [NSString stringWithFormat:@"%@", [dataDic objectForKey:@"aqi"]];
    return aqi;
}

@end
