//
//  AQICNAPIs.h
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 20..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//
// http://aqicn.org/json-api/doc/

#ifndef AQICNAPIs_h
#define AQICNAPIs_h

// 안양 위도 경도 : 37.401112 126.978426
static NSString *const AQICN_API_CITY_FEED = @"https://api.waqi.info/feed/shanghai/?token=";
static NSString *const AQICN_API_GEO_IP_FEED = @"https://api.waqi.info/feed/here/?token="; // (ip based)
static NSString *const AQICN_API_GEO_LAT_LNG_FEED = @"https://api.waqi.info/feed/geo:37.4;126.97/?token="; // (lat/lng based)
static NSString *const AQICN_API_MAP_QUERY = @"https://api.waqi.info/map/bounds/?latlng=39.379436,116.091230,40.235643,116.784382&token=demo";
static NSString *const AQICN_API_SEARCH = @"https://api.waqi.info/search/?token=demo&keyword=bangalore";




#endif /* AQICNAPIs_h */
