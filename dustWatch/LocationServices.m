//
//  LocationServices.m
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 20..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import "LocationServices.h"


@implementation LocationServices

@synthesize locationManager, currentLocation;

- (LocationServices*)initWithDelegate:(id)delegate {
    if (self = [super init]) {
        //do stuff
    }
    if (locationManager == nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = delegate;
    }
    
    return self;
}

- (void)startLocationServices {
//    [locationManager requestLocation];
    
    if ([CLLocationManager locationServicesEnabled]) {
        [locationManager startUpdatingLocation];
    } else {
        NSLog(@"Location services is not enabled");
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = newLocation;
    
    NSLog(@"Latidude %@ Longitude: %@", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    NSLog(@"Update failed with error: %@", error);
}

@end
