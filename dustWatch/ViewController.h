//
//  ViewController.h
//  dustWatch
//
//  Created by Myounghoon Jo on 2018. 5. 7..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *m_resultLabel;
@property (weak, nonatomic) IBOutlet UIButton *m_btnCityFeed;

- (IBAction)cityFeedClicked:(id)sender;
- (IBAction)getLocationClicked:(id)sender;
- (IBAction)AQIClicked:(id)sender;

@end

