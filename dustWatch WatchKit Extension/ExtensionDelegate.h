//
//  ExtensionDelegate.h
//  dustWatch WatchKit Extension
//
//  Created by Myounghoon Jo on 2018. 5. 7..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate>

@end
