//
//  InterfaceController.m
//  dustWatch WatchKit Extension
//
//  Created by Myounghoon Jo on 2018. 5. 7..
//  Copyright © 2018년 Myounghoon Jo. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController ()

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    NSLog(@"awakeWithContext");
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    NSLog(@"willActivate");
    [self.m_label setText:@"changed"];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
    NSLog(@"didDeactivate");
}

@end



